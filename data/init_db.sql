--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1 (Debian 15.1-1.pgdg110+1)
-- Dumped by pg_dump version 15.1 (Debian 15.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: location; Type: TYPE; Schema: public; Owner: username
--

CREATE TYPE public.location AS ENUM (
    'home',
    'street',
    'gym'
);


ALTER TYPE public.location OWNER TO username;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: exercises; Type: TABLE; Schema: public; Owner: username
--

CREATE TABLE public.exercises (
    id integer NOT NULL,
    description character varying NOT NULL,
    "MET" double precision NOT NULL,
    locations public.location[] NOT NULL,
    positive_impact_on_disease jsonb DEFAULT '{}'::jsonb NOT NULL,
    negative_impact_on_disease jsonb DEFAULT '{}'::jsonb NOT NULL
);


ALTER TABLE public.exercises OWNER TO username;

--
-- Name: exercises_id_seq; Type: SEQUENCE; Schema: public; Owner: username
--

CREATE SEQUENCE public.exercises_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.exercises_id_seq OWNER TO username;

--
-- Name: exercises_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: username
--

ALTER SEQUENCE public.exercises_id_seq OWNED BY public.exercises.id;


--
-- Name: exercises id; Type: DEFAULT; Schema: public; Owner: username
--

ALTER TABLE ONLY public.exercises ALTER COLUMN id SET DEFAULT nextval('public.exercises_id_seq'::regclass);


--
-- Data for Name: exercises; Type: TABLE DATA; Schema: public; Owner: username
--

COPY public.exercises (id, description, "MET", locations, positive_impact_on_disease, negative_impact_on_disease) FROM stdin;
0	название	0	{}	{}	{}
\.


--
-- Name: exercises_id_seq; Type: SEQUENCE SET; Schema: public; Owner: username
--

SELECT pg_catalog.setval('public.exercises_id_seq', 1, false);


--
-- Name: exercises exercises_pkey; Type: CONSTRAINT; Schema: public; Owner: username
--

ALTER TABLE ONLY public.exercises
    ADD CONSTRAINT exercises_pkey PRIMARY KEY (id);


--
-- Name: ix_exercises_id; Type: INDEX; Schema: public; Owner: username
--

CREATE INDEX ix_exercises_id ON public.exercises USING btree (id);


--
-- Name: ix_exercises_negative_impact_on_disease; Type: INDEX; Schema: public; Owner: username
--

CREATE INDEX ix_exercises_negative_impact_on_disease ON public.exercises USING btree (negative_impact_on_disease);


--
-- Name: ix_exercises_positive_impact_on_disease; Type: INDEX; Schema: public; Owner: username
--

CREATE INDEX ix_exercises_positive_impact_on_disease ON public.exercises USING btree (positive_impact_on_disease);


--
-- PostgreSQL database dump complete
--

