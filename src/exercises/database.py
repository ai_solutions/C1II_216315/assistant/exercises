from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker

from exercises.settings import get_settings


settings = get_settings()
engine = create_async_engine(
    settings.DB_URL,
    # echo=True,
)
async_session = async_sessionmaker(
    engine,
    # autocommit=False,
    # autoflush=False
    expire_on_commit=False,
)
