from typing import Literal

from pydantic import BaseModel

from exercises.types import Location


class Exercise(BaseModel):
    """
    tuple of `id` and `location` is always unique
    """

    id: int
    description: str
    MET: float

    locations: set[Location]

    positive_impact_on_disease: dict[str, int]
    negative_impact_on_disease: dict[str, int]

    # activity_type: ActivityType
    # involved_muscles: list[int]
    # sensor_data:
    # fitness_level:
    # continuum_level:

    class Config:
        orm_mode = True


# class Muscles(BaseModel):
#     id: int
#     group: str
#     location: str
#     body_part: str
#
#     class Config:
#         orm_mode = True


class ExerciseFilter(BaseModel):
    id: int | None = None
    description: str | None = None
    MET: float | None = None
    MET_operation: Literal["==", "<", "<=", ">=", ">"] = "=="
    locations: list[Location] | None = None
    positive_diseases: list[str] | None = None
    negative_diseases: list[str] | None = None
