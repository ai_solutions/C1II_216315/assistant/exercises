from sqlalchemy import Enum, text
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, validates
from sqlalchemy.ext.mutable import MutableDict, MutableList
from sqlalchemy.dialects.postgresql import ARRAY, JSONB

from exercises.types import Location  # , ActivityType


class Base(DeclarativeBase):
    pass


class Exercise(Base):
    __tablename__ = "exercises"

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    description: Mapped[str]
    MET: Mapped[float]

    locations: Mapped[list[Location]] = mapped_column(
        MutableList.as_mutable(ARRAY(Enum(Location)))  # type: ignore
    )

    @validates("locations")
    def validate_my_column(self, key, value):
        if len(value) != len(set(value)):
            raise ValueError("Duplicate values not allowed")
        return value

    positive_impact_on_disease: Mapped[JSONB] = mapped_column(
        MutableDict.as_mutable(JSONB),  # type: ignore
        server_default=text("'{}'::jsonb"),
        index=True,
    )
    negative_impact_on_disease: Mapped[JSONB] = mapped_column(
        MutableDict.as_mutable(JSONB),  # type: ignore
        server_default=text("'{}'::jsonb"),
        index=True,
    )

    # activity_type: Mapped[ActivityType]
    # involved_muscles: Mapped[list[int]]
    # sensor_data: Mapped[]

    # fitness_level: Mapped[]
    # continuum_level: Mapped[]

    def __repr__(self) -> str:
        return (
            "Exercise("
            f"id={self.id!r}, "
            f"description={self.description!r}, "
            f"MET={self.MET!r}, "
            f"localtions={self.locations!r}, "
            f"positive_impact_on_disease={self.positive_impact_on_disease!r}, "
            f"negative_impact_on_disease={self.negative_impact_on_disease!r}"
            ")"
        )


# class Muscles(Base):
#     __tablename__ = "muscles"

#     id: Mapped[int] = mapped_column(primary_key=True, index=True)
#     group: Mapped[str]
#     location: Mapped[str]
#     body_part: Mapped[str]
