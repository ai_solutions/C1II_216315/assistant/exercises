from functools import lru_cache

from pydantic import BaseSettings, PostgresDsn


class Settings(BaseSettings):
    DB_URL: PostgresDsn


@lru_cache()
def get_settings() -> Settings:
    return Settings()  # type: ignore
