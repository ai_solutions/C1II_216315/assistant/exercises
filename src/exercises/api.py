from typing import Literal
from fastapi import FastAPI, Depends, Query, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware

from exercises import schemas, crud
from exercises.database import async_session
from exercises.types import Location

# needed for startup function
# from exercises import models
# from exercises.database import engine


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


async def get_session():
    async with async_session() as session:
        yield session


# @app.on_event("startup")
# async def startup():
#     async with engine.begin() as conn:
#         await conn.run_sync(models.Base.metadata.drop_all)
#         await conn.run_sync(models.Base.metadata.create_all)


@app.get("/exercises", response_model=list[schemas.Exercise])
async def read_exercises(session=Depends(get_session)):
    return await crud.get_exercises(session)


# get by property


@app.post("/exercises", response_model=list[schemas.Exercise])
async def read_exercises_filtered(
    body: schemas.ExerciseFilter, session=Depends(get_session)
):
    # fill control to filter function
    return await crud.get_exercises_filtered(
        session,
        **dict(body),
    )


@app.get("/exercises/id/{id}", response_model=schemas.Exercise)
async def read_exercises_by_id(id: int, session=Depends(get_session)):
    # id
    exercise_res = list(await crud.get_exercises_filtered(session, id=id))
    if len(exercise_res) == 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="No exercises with this ID exist",
        )
    elif len(exercise_res) == 1:
        return exercise_res[0]
    elif len(exercise_res) > 1:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Multiple exercises found",
        )


@app.get("/exercises/description/{description}", response_model=list[schemas.Exercise])
async def read_exercises_by_description(description: str, session=Depends(get_session)):
    # description
    return await crud.get_exercises_filtered(session, description=description)


@app.get("/exercises/MET/{MET}", response_model=list[schemas.Exercise])
async def read_exercises_by_MET(
    MET: float,
    session=Depends(get_session),
):
    # MET
    return await crud.get_exercises_filtered(session, MET=MET)


@app.get("/exercises/MET/{MET}/{operation}", response_model=list[schemas.Exercise])
async def read_exercises_by_MET_with_operation(
    MET: float,
    operation: Literal["==", "<", "<=", ">=", ">"] = "==",
    session=Depends(get_session),
):
    # MET with operation
    return await crud.get_exercises_filtered(session, MET=MET, MET_operation=operation)


@app.get("/exercises/location/{location}", response_model=list[schemas.Exercise])
async def read_exercises_by_location(location: Location, session=Depends(get_session)):
    # location
    return await crud.get_exercises_filtered(session, locations=[location])


@app.get("/exercises/locations", response_model=list[schemas.Exercise])
async def read_exercises_by_locations(
    locations: list[Location] = Query(), session=Depends(get_session)
):
    # locations
    return await crud.get_exercises_filtered(session, locations=locations)


@app.get(
    "/exercises/positive_impact_on/{disease}", response_model=list[schemas.Exercise]
)
async def read_exercises_by_positive_impact(disease: str, session=Depends(get_session)):
    print(disease)
    # positive_impact_on_disease
    return await crud.get_exercises_filtered(session, positive_diseases=[disease])


@app.get("/exercises/positive_impact_on", response_model=list[schemas.Exercise])
async def read_exercises_by_positive_impacts(
    diseases: list[str] = Query(), session=Depends(get_session)
):
    # positive_impact_on_diseases
    return await crud.get_exercises_filtered(session, positive_diseases=diseases)


@app.get(
    "/exercises/negative_impact_on/{disease}", response_model=list[schemas.Exercise]
)
async def read_exercises_by_negative_impact(disease: str, session=Depends(get_session)):
    # negative_impact_on_disease
    return await crud.get_exercises_filtered(session, negative_diseases=[disease])


@app.get("/exercises/negative_impact_on", response_model=list[schemas.Exercise])
async def read_exercises_by_negative_impacts(
    diseases: list[str] = Query(), session=Depends(get_session)
):
    # negative_impact_on_diseases
    return await crud.get_exercises_filtered(session, negative_diseases=diseases)
