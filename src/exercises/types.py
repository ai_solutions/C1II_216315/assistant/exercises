from enum import Enum, IntEnum


# class ActivityType(IntEnum):
#     aerobic = 0
#     anaerobic = 1


class Location(str, Enum):
    home = "home"
    street = "street"
    gym = "gym"
