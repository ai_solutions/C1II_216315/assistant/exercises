from typing import Iterable, Literal

from sqlalchemy import select
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.asyncio import AsyncSession

from exercises.models import Exercise
from exercises.types import Location

# exercises
async def get_exercises(session: AsyncSession) -> Iterable[Exercise]:
    exercises = await session.execute(select(Exercise))
    return exercises.scalars().all()


async def get_exercises_filtered(
    session: AsyncSession,
    *,
    id: int | None = None,
    description: str | None = None,
    MET: float | None = None,
    MET_operation: Literal["==", "<", "<=", ">=", ">"] = "==",
    locations: list[Location] | None = None,
    positive_diseases: list[str] | None = None,
    negative_diseases: list[str] | None = None,
) -> Iterable[Exercise]:
    operation = select(Exercise)

    if id:
        operation = operation.where(Exercise.id == id)
    if description:
        operation = operation.where(Exercise.description == description)
    if MET:
        if MET_operation == "<":
            operation = operation.where(Exercise.MET < MET)
        elif MET_operation == "<=":
            operation = operation.where(Exercise.MET <= MET)
        elif MET_operation == "==":
            operation = operation.where(Exercise.MET == MET)
        elif MET_operation == ">=":
            operation = operation.where(Exercise.MET >= MET)
        elif MET_operation == ">":
            operation = operation.where(Exercise.MET > MET)
    if locations:
        operation = operation.where(Exercise.locations.contains(locations))
    if positive_diseases:
        for positive_diseas in positive_diseases:
            operation = operation.where(
                # checks that column has key
                Exercise.positive_impact_on_disease.contains(positive_diseas)
            )
    if negative_diseases:
        for negative_diseas in negative_diseases:
            operation = operation.where(
                # checks that column has key
                Exercise.negative_impact_on_disease.contains(negative_diseas)
            )

    exercises = await session.execute(operation)
    return exercises.scalars().all()
