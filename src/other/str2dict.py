#!/usr/bin/env python3

def str2dict(inp: str) -> str:
    res = "{"
    add = True
    for el in inp.split(","):
        if "-" in el:
            k, v, *other = el.split("-")

            if add:
                res += f"'{k.strip()}': {v.strip()},"
            else:
                res += f"{k.strip()}': {v.strip()},"
                add = True

        else:
            res += f"'{el.strip()}, "
            add = False
    res += "}"
    return res

if __name__ == "__main__":
    while True:
        inp = input()
        print()
        print(str2dict(inp))
        print()
